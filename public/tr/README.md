## MESHINE SWARM TECHNOLOGIES 
#### WEBSITE DOCS

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents** 
<!--  *generated with [DocToc](https://github.com/thlorenz/doctoc)* -->

- [Mainpage](landing)
- [Projets](projects)
- [Services](services)
- [About](about)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Site divided into two languages. 
So site codes written both english `/eng` and turkish under `/tr`  folders.

#### Landing Page
<!-- [GitLab CI][ci]
[`.gitlab-ci.yml`](.gitlab-ci.yml) -->

All things get stared on `/index.html` file under both lang directories. 
Includes **landing page**.

#### Projects
Located on  `/projects.html` under languages directories.

Read more about [Project Page][projects].

#### Services

Located on  `/services.html` under languages directories.

Read more about [Services Page][services].

#### About
This one included two pages inside in its dropdown menu.
One of them is located on  `/about.html` under languages directories.

Read more about [About Page][about].

Other one is located on `/contacts.html` under languages directories.

Read more about [Contact Page][contact].

[landing]: https://www.meshine.tech/tr/index.html
[projects]: https://www.meshine.tech/tr/projects.html
[services]: https://www.meshine.tech/tr/services.html
[about]: https://www.meshine.tech/tr/about.html
[contact]: https://www.meshine.tech/tr/contact.html